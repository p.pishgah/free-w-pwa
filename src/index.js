import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorkerRegistration from './serviceWorkerRegistration';
import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://cra.link/PWA
serviceWorkerRegistration.register();

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

/** codes for add to home screen button **/
let deferredPrompt;
const addBtn = document.querySelector('.add-button');
addBtn.style.display = 'none';
console.log('111')

window.addEventListener('beforeinstallprompt', (e) => {
    console.log('222')
    console.log('time for open app alert')


    // Prevent Chrome 67 and earlier from automatically showing the prompt
    e.preventDefault();
    // Stash the event so it can be triggered later.
    deferredPrompt = e;
    // Update UI to notify the user they can add to home screen
    addBtn.style.display = 'block';
    console.log('333')


    addBtn.addEventListener('click', () => {
        console.log('444')

        // hide our user interface that shows our A2HS button
        addBtn.style.display = 'none';
        // Show the prompt
        deferredPrompt.prompt();
        console.log('555')

        // Wait for the user to respond to the prompt
        deferredPrompt.userChoice.then((choiceResult) => {
            console.log('666')
            if (choiceResult.outcome === 'accepted') {
                console.log('User accepted the A2HS prompt');
                console.log('777')

            } else {
                console.log('User dismissed the A2HS prompt');
                console.log('888')

            }
            deferredPrompt = null;
            console.log('999')

        });
    });
});