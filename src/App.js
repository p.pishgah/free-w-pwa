import React from 'react';
import {BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import 'antd/dist/antd.css';
import s from './app.module.css';
import HomePage from './routes/homePage/homePage';
import AboutUs from './routes/aboutUs/aboutUs';
import PageThree from "./routes/pageThree/pageThree";

function App() {
    return (
        <Router>
            <div className={s.body}>
                {/*<TopHeader />*/}
                <Switch>
                    <Route path="/" component={HomePage} exact/>
                    <Route path="/about-us" component={AboutUs}/>
                    <Route path="/page3" component={PageThree}/>
                </Switch>
            </div>
        </Router>
    );
}

export default App;