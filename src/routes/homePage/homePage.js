import React from 'react';
import s from './homePage.module.css';
import { Button } from 'antd';
import classNames from 'classnames';

const homePage = () => {
    return(
        <div className={s.HomepageBody}>
            <div className={s.wifiTitle}>
                free wifi sharing system
            </div>
            <Button type="default" className={classNames('add-button',s.installBtn)}>install</Button>
        </div>
    )
};

export default homePage;